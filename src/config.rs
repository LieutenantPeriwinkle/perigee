use std::fs;

use anyhow::{anyhow, Result};
use chrono::{Local, NaiveDate};
use ramhorns::Content;
use serde::{Deserialize, Serialize};
use toml::{from_str, to_string_pretty};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct FileConfig {
    pub page_title: Option<String>,
    pub title: String,
    pub draft: Option<bool>,
    pub date: Option<NaiveDate>,
    pub desc: Option<String>,
    pub head_frags: Option<Vec<String>>,
    pub body_frags: Option<Vec<String>>,
    pub post_body_frags: Option<Vec<String>>,
    // to be continued...
}

impl FileConfig {
    pub fn new() -> Self {
        Self {
            page_title: None,
            title: String::new(),
            draft: None,
            date: None,
            desc: None,
            head_frags: None,
            body_frags: None,
            post_body_frags: None,
        }
    }
    pub fn new_page() -> Self {
        Self {
            page_title: None,
            title: String::new(),
            draft: Some(true),
            date: Some(Local::now().date_naive()),
            desc: None,
            head_frags: None,
            body_frags: None,
            post_body_frags: None,
        }
    }
    pub fn ser(&self) -> Result<String> {
        Ok(format!("---\n{}---\n", to_string_pretty(self)?))
    }
    pub fn delimited_de(s: &str) -> Result<Self> {
        let split = s
            .split_once("---\n")
            .ok_or_else(|| anyhow!("Config opening delimiter not found!"))?
            .1
            .split_once("---\n")
            .ok_or_else(|| anyhow!("Config closing delimiter not found!"))?
            .0;
        Self::de(split)
    }
    pub fn de(s: &str) -> Result<Self> {
        from_str(s).map_err(|e| e.into())
    }
    pub fn update_dir(&mut self, other: &FileConfig) {
        if self.page_title.is_none() {
            self.page_title = other.page_title.clone();
        }
        if self.desc.is_none() {
            self.desc = other.desc.clone();
        }
        self.draft = self.draft.or(other.draft);
        self.date = self.date.or(other.date);
        if let Some(ref mut h) = self.head_frags {
            for i in other.head_frags.as_ref().unwrap_or(&vec![]) {
                h.push(i.clone());
            }
        }
        if let Some(ref mut h) = self.post_body_frags {
            for i in other.post_body_frags.as_ref().unwrap_or(&vec![]) {
                h.push(i.clone());
            }
        }
        if let Some(ref mut h) = self.body_frags {
            for i in other.body_frags.as_ref().unwrap_or(&vec![]) {
                h.push(i.clone());
            }
        }
    }
}

#[derive(Deserialize, Debug)]
pub struct SiteConfig {
    base_url: String,
    page_title: Option<String>,
    pub output_dir: Option<String>,
    page_subdirs: Option<bool>,
    copyright: Option<String>,
    head_frags: Option<Vec<String>>,
    body_frags: Option<Vec<String>>,
    post_body_frags: Option<Vec<String>>,
}

impl SiteConfig {
    pub const DEFAULT_CONFIG: &[u8] =
        br#"base_url = "" # the url of the site, e.g. https://example.org
copyright = "" # The copyright text, e.g. (c) 2022 LieutenantPeriwinkle
head_frags = [] # fragments used inside the <head>
body_frags = [] # fragments used before page content, inside <body>
post_body_frags = [] # fragments used after the page content
"#;
    pub fn open() -> Result<Self> {
        from_str(&fs::read_to_string("config.toml")?).map_err(|e| e.into())
    }
}

#[derive(Default, Content)]
pub struct RenderConfig {
    base_url: String,
    copyright: Option<String>,
    pub page_subdirs: bool,
    #[ramhorns(skip)]
    pub head_frags: Vec<String>,
    #[ramhorns(skip)]
    pub body_frags: Vec<String>,
    #[ramhorns(skip)]
    pub post_body_frags: Vec<String>,
    page_title: Option<String>,
    title: String,
    date: Option<String>,
    desc: Option<String>,
    #[ramhorns(skip)]
    num_file_frags: (usize, usize, usize),
    #[ramhorns(skip)]
    num_dir_frags: (usize, usize, usize),
    #[ramhorns(skip)]
    global_page_title: Option<String>,
    #[ramhorns(skip)]
    dir_page_title: Option<String>,
}

impl RenderConfig {
    pub fn new(site: SiteConfig) -> Self {
        Self {
            base_url: site.base_url,
            copyright: site.copyright,
            global_page_title: site.page_title,
            page_subdirs: site.page_subdirs.unwrap_or(false),
            head_frags: site.head_frags.unwrap_or_default(),
            body_frags: site.body_frags.unwrap_or_default(),
            post_body_frags: site.post_body_frags.unwrap_or_default(),
            ..Default::default()
        }
    }
    fn pop_file_frags(&mut self) {
        if self.num_file_frags == (0, 0, 0) {
            return;
        }
        self.head_frags
            .truncate(self.head_frags.len() - self.num_file_frags.0);
        self.body_frags
            .truncate(self.body_frags.len() - self.num_file_frags.1);
        self.post_body_frags
            .truncate(self.post_body_frags.len() - self.num_file_frags.2);
        self.num_file_frags = (0, 0, 0);
    }
    pub fn update_file(&mut self, file: FileConfig) {
        self.pop_file_frags();
        let mut fhf = file.head_frags.unwrap_or_default();
        self.num_file_frags.0 = fhf.len();
        self.head_frags.append(&mut fhf);

        fhf = file.body_frags.unwrap_or_default();
        self.num_file_frags.1 = fhf.len();
        self.body_frags.append(&mut fhf);

        fhf = file.post_body_frags.unwrap_or_default();
        self.num_file_frags.2 = fhf.len();
        self.post_body_frags.append(&mut fhf);

        self.page_title = file.page_title.or(self.dir_page_title.clone());
        self.title = file.title;
        self.date = file.date.map(|d| d.to_string());
        self.desc = file.desc;
    }
    pub fn update_dir(&mut self, dir: &FileConfig) {
        self.pop_file_frags();
        self.dir_page_title = dir.page_title.clone().or(self.global_page_title.clone());
        self.head_frags
            .truncate(self.head_frags.len() - self.num_dir_frags.0);
        self.body_frags
            .truncate(self.body_frags.len() - self.num_dir_frags.1);
        self.post_body_frags
            .truncate(self.post_body_frags.len() - self.num_dir_frags.2);
        self.date = dir.date.map(|d| d.to_string());
    }
}
