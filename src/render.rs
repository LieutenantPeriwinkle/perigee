use crate::config::{FileConfig, RenderConfig, SiteConfig};

use anyhow::Result;
use comrak::{
    markdown_to_html, ComrakExtensionOptions, ComrakOptions, ComrakRenderOptions, ListStyleType,
};
use ramhorns::{Ramhorns, Template};
use std::{
    collections::{HashMap, HashSet},
    fs::{copy, create_dir_all, metadata, read_to_string, remove_dir_all, File},
    io::Write,
    path::PathBuf,
};

#[derive(Debug)]
pub struct FileAction {
    path: PathBuf,
    act: ActionType,
}

#[derive(Debug)]
enum ActionType {
    Copy,
    BuildPage,
}

impl FileAction {
    pub fn build_page(path: PathBuf) -> Self {
        Self {
            path,
            act: ActionType::BuildPage,
        }
    }
    pub fn copy(path: PathBuf) -> Self {
        Self {
            path,
            act: ActionType::Copy,
        }
    }
}

pub fn render_site(actions: &[FileAction], drafts: bool, site_conf: SiteConfig) -> Result<()> {
    let mut created_dirs = HashSet::new();
    let mut draft_dirs = HashSet::new();
    let mut dir_conf_cache = HashMap::new();
    let p = PathBuf::from(
        site_conf
            .output_dir
            .clone()
            .unwrap_or_else(|| String::from("public")),
    );
    let c = PathBuf::from("content");
    let opts = ComrakOptions {
        render: ComrakRenderOptions {
            list_style: ListStyleType::Star,
            unsafe_: true,
            ..Default::default()
        },
        extension: ComrakExtensionOptions {
            table: true,
            tasklist: true,
            footnotes: true,
            header_ids: Some(String::new()),
            front_matter_delimiter: Some(String::from("---")),
            strikethrough: true,
            ..Default::default()
        },
        ..Default::default()
    };
    let mut html_output;

    let mut ram: Ramhorns = Ramhorns::lazy("./fragments")?;
    let mut ren_conf = RenderConfig::new(site_conf);

    for i in actions {
        let mut public_path = p.join(&i.path);
        let dirpath = p.join(&i.path);
        let dirpath = dirpath.parent().unwrap_or(&p).clone();
        let content_path = c.join(&i.path);
        if draft_dirs.contains(dirpath) {
            continue;
        } else if created_dirs.insert(PathBuf::from(dirpath)) {
            create_dir_all(dirpath)?;
        }
        match i.act {
            ActionType::Copy => {
                let public_meta = metadata(&public_path);
                if let Ok(public_meta) = public_meta {
                    let public_mtime = public_meta.modified().or(public_meta.created())?;
                    let content_meta = metadata(&public_path)?;
                    let content_mtime = content_meta.modified().or(content_meta.created())?;
                    if public_mtime.duration_since(content_mtime).is_ok() {
                        continue;
                    }
                }
                if content_path.file_name().map(|f| f.to_str()) != Some(Some("config.toml")) {
                    println!(
                        "Copying {} to {}.",
                        content_path.display(),
                        public_path.display()
                    );
                    copy(content_path, public_path)?;
                }
            }
            ActionType::BuildPage => {
                let dirconf_path = content_path.parent().unwrap().join("config.toml");
                let parents = dirconf_path.ancestors().skip(1).collect::<Vec<_>>();
                let mut init;
                let dir_cfg = if let Some(d) = dir_conf_cache.get(&dirconf_path) {
                    d
                } else {
                    init = FileConfig::new();
                    for dir in parents.iter().rev().skip(2) {
                        let dir = dir.join("config.toml");
                        if let Some(d) = dir_conf_cache.get(&dir) {
                            init.update_dir(d);
                        } else {
                            init.update_dir(&if dir.exists() {
                                FileConfig::de(&read_to_string(&dir)?)?
                            } else {
                                FileConfig::new()
                            });
                        };
                    }
                    dir_conf_cache.insert(dirconf_path, init.clone());
                    &init
                };
                let file_contents = read_to_string(&content_path)?;
                let file_cfg = FileConfig::delimited_de(&file_contents)?;
                if !drafts && (file_cfg.draft.unwrap_or(false) || dir_cfg.draft.unwrap_or(false)) {
                    if dirpath != p
                        && content_path.file_name().map(|f| f.to_str()) == Some(Some("index.md"))
                    {
                        draft_dirs.insert(PathBuf::from(dirpath));
                    }
                    println!("Page {} is draft, skipping.", content_path.display());
                    continue;
                }
                if ren_conf.page_subdirs {
                    if public_path.file_name().unwrap().to_ascii_lowercase() != "index.md" {
                        let fs = public_path.file_stem().unwrap().to_owned();
                        public_path = public_path.parent().unwrap().to_owned();
                        public_path.push(fs);
                        create_dir_all(&public_path)?;
                        public_path.push("index.html");
                    }
                }
                public_path.set_extension("html");
                let public_meta = metadata(&public_path);
                if let Ok(public_meta) = public_meta {
                    let public_mtime = public_meta.modified().or(public_meta.created())?;
                    if let Ok(content_meta) = metadata(&content_path) {
                        let content_mtime = content_meta.modified().or(content_meta.created())?;
                        if public_mtime.duration_since(content_mtime).is_ok() {
                            continue;
                        }
                    }
                }
                println!("Building page {}.", public_path.display());
                ren_conf.update_dir(&dir_cfg);
                ren_conf.update_file(file_cfg);
                let rendered_contents = Template::new(file_contents.as_str())?.render(&ren_conf);
                html_output = markdown_to_html(&rendered_contents, &opts);
                let mut output_file = File::create(public_path)?;
                output_file.write_all(b"<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n")?;
                render_frags(&ren_conf.head_frags, &mut ram, &ren_conf, &mut output_file)?;
                output_file.write_all(b"\n</head>\n<body>\n")?;
                render_frags(&ren_conf.body_frags, &mut ram, &ren_conf, &mut output_file)?;
                output_file.write_all(html_output.as_bytes())?;
                render_frags(
                    &ren_conf.post_body_frags,
                    &mut ram,
                    &ren_conf,
                    &mut output_file,
                )?;
                output_file.write_all(b"\n</body>\n</html>")?;
            }
        }
    }
    if !draft_dirs.is_empty() {
        println!("Cleaning up.");
    }
    for d in draft_dirs {
        remove_dir_all(d)?;
    }
    Ok(())
}

fn render_frags(
    frags: &[String],
    rh: &mut Ramhorns,
    rc: &RenderConfig,
    writer: &mut impl Write,
) -> Result<()> {
    for i in frags.iter() {
        let mut i = i.clone();
        if !i.ends_with(".html") {
            i.push_str(".html");
        }
        let tpl = rh.from_file(&i)?;
        tpl.render_to_writer(writer, &rc)?;
    }
    writer.write_all(b"\n")?;
    Ok(())
}
