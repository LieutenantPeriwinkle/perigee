# perigee
"dynamic-friendly static site generation"

This project was started because of Hugo, it just annoyed me in a lot of small ways and had too many things. A lot of its behavior is suspiciously (or not) documented,
and sometimes pretty confusing, so here we are. The goal of this is to be small and simple and easy, and to work how I expect it to, which may not be how you expect (sorry).

# basics
Perigee renders markdown-based content to html, and renders [ramhorns](https://github.com/maciejhirsz/ramhorns/) templates in both markdown and html
files. Other files will simply be copied to the output directory. Perigee is configured in toml, with one config file at the root of a site
directory and per-file configuration at the top of any markdown or html file. 'Fragments', or smaller snippets added to larger files, are able to be
added configurably inside the `<head>`, before the main content in the `<body>`, and after the content. Any files in the `static` directory are copied
to the output without modification.

# how to use
1. create new site
```
pgee new [path_to_site] [(-n|--name) <name_of_site>]
```
ex:
```
pgee new periwinkle.sh/ -n periwinklesh
```
Both arguments are optional. If no path is specified, it defaults to the current directory, and if no name is specified, it defaults to empty.

2. create content
```
pgee create <path_to_content>
```
ex:
```
pgee create foo/bar/baz.md
```
The content path is relative to and inside the `content` directory in the current site. Necessary subdirectories will be automatically created.

3. render site
```
pgee render [(-d|--render_drafts)] [-o <output_dir>]
```
ex:
```
pgee render -d -o output
```
Render the site to the `output` directory, and render content that is marked as a draft.
