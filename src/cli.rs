use bpaf::Bpaf;

#[derive(Bpaf, Debug, Clone)]
pub enum Subcommand {
    #[bpaf(command)]
    /// Create a new site directory structure.
    New {
        #[bpaf(short, long, fallback(String::new()))]
        /// The name of the new site. Default is empty.
        name: String,
        #[bpaf(positional("PATH"), fallback(String::from(".")))]
        /// The location of the new site. Default is the current directory.
        path: String,
    },
    #[bpaf(command)]
    /// Create a new content file.
    Create {
        #[bpaf(positional("PATH"))]
        /// The path to the new file, relative to `content'.
        path: String,
    },
    #[bpaf(command)]
    /// Render the site.
    Render {
        #[bpaf(short('d'), long)]
        render_drafts: bool,
        #[bpaf(short('o'), fallback(String::from("public")))]
        output_dir: String,
    },
}

#[derive(Bpaf, Debug)]
#[bpaf(options)]
pub struct CommandLine {
    #[bpaf(external)]
    pub subcommand: Subcommand,
}
