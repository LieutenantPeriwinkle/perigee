use anyhow::{ensure, Context, Result};
use std::{
    collections::HashSet,
    fs::{canonicalize, copy, create_dir_all, metadata, read_dir},
    path::{Path, PathBuf},
};
use walkdir::WalkDir;

pub fn check_for_site(path: impl AsRef<Path>) -> Result<bool> {
    let path = canonicalize(path)?;
    let read = read_dir(&path).context(format!("Couldn't read `{}'", path.to_string_lossy()))?;
    let mut config = false;
    let mut content = false;
    let mut frag = false;
    let mut stat = false;
    for entry in read.flatten() {
        let name = entry.file_name();
        let ty = entry.file_type()?;
        if name == "config.toml" && ty.is_file() {
            config = true;
        } else if ty.is_dir() {
            if name == "content" {
                content = true;
            } else if name == "fragments" {
                frag = true;
            } else if name == "static" {
                stat = true;
            }
        }
    }
    Ok(config && content && frag && stat)
}

pub fn check_dir(path: impl AsRef<Path>) -> Result<()> {
    let path = canonicalize(path)?;
    let s = path.to_string_lossy();
    let meta = metadata(&path).context(format!("Couldn't stat `{}'", s))?;
    ensure!(meta.is_dir(), "`{}' is not a directory!", s);
    ensure!(!meta.permissions().readonly(), "We can't write to `{}'!", s);
    Ok(())
}

pub fn move_static() -> Result<()> {
    let p = PathBuf::from("static");
    if !p.exists() {
        return Ok(());
    }
    let walk = WalkDir::new(p);
    let mut created_dirs = HashSet::new();
    let p = PathBuf::from("public");
    for entry in walk
        .into_iter()
        .filter_entry(|e| {
            e.file_name()
                .to_str()
                .map(|s| !s.starts_with('.'))
                .unwrap_or(true)
        })
        .flatten()
    {
        if !entry.file_type().is_dir() {
            let orig_path = entry.into_path();
            let new_path = p.join(orig_path.strip_prefix("static")?);
            let new_meta = metadata(&new_path);
            if let Ok(new_meta) = new_meta {
                let new_mtime = new_meta.modified().or(new_meta.created())?;
                let old_meta = metadata(&orig_path)?;
                let old_mtime = old_meta.modified().or(old_meta.created())?;
                if new_mtime.duration_since(old_mtime).is_ok() {
                    continue;
                }
            }
            let dirpath = new_path.parent().unwrap_or(&p);
            if created_dirs.insert(PathBuf::from(dirpath)) {
                create_dir_all(dirpath)?;
            }
            println!("Copying {} to {}.", orig_path.display(), new_path.display());
            copy(orig_path, new_path)?;
        }
    }
    Ok(())
}
