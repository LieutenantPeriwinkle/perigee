use anyhow::{ensure, Context, Result};
use std::{
    env,
    fs::{self, File},
    io::Write,
    path::PathBuf,
};
use walkdir::WalkDir;

mod cli;
mod config;
mod render;
mod util;
use cli::Subcommand;
use config::{FileConfig, SiteConfig};
use render::{render_site, FileAction};
use util::*;

fn main() -> Result<()> {
    let args = cli::command_line().run();
    match args.subcommand {
        Subcommand::New { path, name } => {
            check_dir(&path)?;
            ensure!(
                !check_for_site(&path)?,
                "`{}' seems to already be a site!",
                path
            );
            env::set_current_dir(&path).context("Couldn't change directory!")?;
            fs::create_dir("content").context("Couldn't create dir `content'!")?;
            fs::create_dir("fragments").context("Couldn't create dir `fragments'!")?;
            fs::create_dir("static").context("Couldn't create dir `static'!")?;
            let mut f = File::create("config.toml").context("Couldn't create `config.toml'!")?;
            f.write_all(format!("page_title = \"{}\"\n", name).as_bytes())
                .context("Couldn't write to `config.toml'!")?;
            f.write_all(SiteConfig::DEFAULT_CONFIG)
                .context("Couldn't write to `config.toml'!")?;
            println!(
                "Created new site \"{}\" at {}.",
                name,
                env::current_dir()?.display()
            );
        }
        Subcommand::Create { path: p } => {
            check_dir(".")?;
            ensure!(check_for_site(".")?, "We aren't in a site directory!");
            let path = PathBuf::from("content").join(&p);
            ensure!(
                path.is_relative(),
                "Path must be a relative path to a file!"
            );
            ensure!(
                !path.exists(),
                format!("{} already exists!", path.display())
            );
            let mut dirpath = path.clone();
            dirpath.pop();
            fs::create_dir_all(&dirpath).context(format!(
                "Could not create directory `{}'!",
                dirpath.display()
            ))?;
            let mut f = File::create(&path)
                .context(format!("Could not create file `{}'!", path.display()))?;
            let config = FileConfig::new_page();
            f.write_all(config.ser()?.as_bytes())
                .context(format!("Could not write to file `{}'!", path.display()))?;
            println!("Created new file {}.", path.display());
        }
        Subcommand::Render {
            render_drafts: draft,
            output_dir,
        } => {
            ensure!(check_for_site(".")?, "We aren't in a site directory!");
            let w = WalkDir::new("content");
            let mut actions: Vec<FileAction> = vec![];
            let mut site_conf = SiteConfig::open()?;
            site_conf.output_dir = site_conf.output_dir.or(Some(output_dir));
            for entry in w
                .into_iter()
                .filter_entry(|e| {
                    e.file_name()
                        .to_str()
                        .map(|s| !s.starts_with('.'))
                        .unwrap_or(true)
                })
                .flatten()
            {
                if !entry.file_type().is_dir() {
                    let p = entry.into_path().strip_prefix("content")?.to_owned();
                    let ext = p.extension().map(|s| s.to_string_lossy().to_string());
                    actions.push(match ext.unwrap_or_default().as_str() {
                        "md" | "html" => FileAction::build_page(p),
                        _ => FileAction::copy(p),
                    });
                }
            }
            render_site(&actions, draft, site_conf)?;
            move_static()?;
        }
    }
    Ok(())
}
